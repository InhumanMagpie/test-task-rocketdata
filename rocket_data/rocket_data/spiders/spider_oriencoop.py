import re

import scrapy
from scrapy.http import Response




class Spider(scrapy.Spider):
    url = "https://oriencoop.cl"
    name = "spider_oriencoop"
    mapping = {
        "dirección": "address",
        "teléfono": "phones",
        "agente": "name",
        "horarios": "working_hours",
    }

    def start_requests(self):
        yield scrapy.Request(f"{self.url}/sucursales.htm", callback=self.parse)

    def parse(self, response):
        links = response.xpath(
            "//ul[contains(@class, 'c-list c-accordion')]/li/ul/li/a/@href"
        ).getall()

        for link in links:
            yield scrapy.Request(f"{self.url}{link}", callback=self.parse_link)

    def parse_link(self, response: Response):
        items = response.xpath("//div[contains(@class, 's-dato')]/p")
        data = {}
        for i in items:
            key = i.xpath(".//strong/text()").get().lower().strip(" :")
            en_key = self.mapping[key]
            value = i.xpath(".//span/text()").getall()
            if en_key == "address" or en_key == "name":
                value = value[0]
            data[en_key] = value
        latlon = response.xpath("//div[contains(@class, 's-mapa')]/iframe/@src").get()
        data["latlon"] = tuple(
            map(float, re.findall(r"!2d(-?\d+.\d+)!3d(-?\d+.\d+)", latlon)[0])
        )
        yield data

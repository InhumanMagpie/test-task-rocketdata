import json
import re

import scrapy

from rocket_data.items import SpiderItem


class Spider(scrapy.Spider):
    url = "https://naturasiberica.ru"
    name = "naturasiberica"

    def start_requests(self):
        yield scrapy.Request(f"{self.url}/our-shops/", callback=self.parse)

    def parse(self, response):
        headers = {
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
            "X-Requested-With": "XMLHttpRequest",
        }

        yield scrapy.Request(
            f"{self.url}/local/php_interface/ajax/getShopsData.php",
            callback=self.parse_data,
            method="POST",
            body="type=all",
            headers=headers,
        )

    def parse_data(self, response):
        items = json.loads(response.text)
        clean_items = [
            *self.clean_list(items["original"]),
            *self.clean_list(items["usual"][0]["cities"][0]["shops"]),
            *self.clean_list(items["usual"][1]["cities"][0]["shops"]),
            *self.clean_list(items["usual"][1]["cities"][1]["shops"]),
            *self.clean_list(items["usual"][0]["cities"][0]["shops"]),
        ]
        for i in clean_items:
            yield {
                "address": i.get("address"),
                "latlon": list(
                    ((float(i["location"]["lat"]), float(i["location"]["lng"])))
                )
                if i.get("location")
                else None,
                "name": i.get("name"),
                "phones": [i.get("phone")] if i["phone"] else None,
                "working_hours": i.get("schedule"),
            }

    def clean_list(self, original_items):
        return [
            {
                k: v
                for k, v in dict_.items()
                if k not in ("id", "country", "city", "images", "website")
            }
            for dict_ in original_items
        ]


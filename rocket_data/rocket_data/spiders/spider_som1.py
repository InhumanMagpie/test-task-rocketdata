import re

import scrapy
from scrapy.http import Response


class Spider(scrapy.Spider):
    url = "https://som1.ru"
    name = "spider_som"
    mapping = {
        "адрес": "address",
        "телефон": "phones",
        "имя": "name",
        "время работы": "working_hours",
    }

    def start_requests(self):
        yield scrapy.Request(f"{self.url}/shops/", callback=self.parse, method="POST")

    def parse(self, response):
        city_ids = response.xpath("//input[@name = 'CITY_ID']/@id").getall()

        for city in city_ids:
            yield scrapy.FormRequest(
                f"{self.url}/shops/",
                method="POST",
                callback=self.parse_city,
                formdata={"CITY_ID": city},
            )

    def parse_city(self, response):
        shop_urls = response.xpath(
            "//div[contains(@class, 'shops-col shops-button')]/a/@href"
        ).getall()
        for shop_url in shop_urls:
            yield scrapy.Request(f"{self.url}{shop_url}", callback=self.parse_shop)

    def parse_shop(self, response: Response):
        items = response.xpath("//table[contains(@class, 'shop-info-table')]/tr")
        data = {}
        data["name"] = response.xpath(
            "//div[contains(@class,'container')]/h1/text()"
        ).get()
        for i in items:
            key = i.xpath(".//td/b/text()").get().lower().strip(" :")
            en_key = self.mapping[key]
            value = i.xpath(".//td[3]/text()").get()
            if en_key == "phones" or en_key == "working_hours":
                value = value.split(",")
            data[en_key] = value
        latlon = response.xpath(
            "//script[contains(text(),'showShopsMap')]/text()"
        ).get()
        data["latlon"] = tuple(map(float, re.findall(r"(\d+.\d+)", latlon)))

        yield data

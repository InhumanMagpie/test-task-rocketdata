# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy import Item, Field


class SpiderItem(Item):
    address = Field()
    latlon = Field()
    name = Field()
    phones = Field()
    working_hours = Field()


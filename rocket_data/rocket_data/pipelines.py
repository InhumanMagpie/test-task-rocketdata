import json


class JsonPipeline:
    def process_item(self, item, spider):
        self.data.append(item)
        return item

    def open_spider(self, spider):
        self.data = []

    def close_spider(self, spider):
        with open("items.json", "w", encoding="utf-8") as f:
            json.dump(self.data, f, ensure_ascii=False, indent=4)
